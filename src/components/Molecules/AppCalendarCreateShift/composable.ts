export const useLocalHelpers = () => {
    const init_state = {
        start_time: '',
        end_time: '',
        comment: '',
        type: '',
        status: '',
        name: '',
        shift_user_id: '',
        location: '',
        isEnded: false,
    };

    return {
        init_state,
    };
};
