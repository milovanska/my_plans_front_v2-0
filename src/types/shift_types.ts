export interface TShift {
    id?: number | undefined;
    name: string | undefined;
    comment: string;
    end_at?: string | undefined;
    end_time: string;
    isEnded?: boolean;
    is_anomaly_time?: boolean;
    location: string;
    month?: number | null;
    shift_user_id?: number | null;
    start_at?: string;
    start_time: string;
    type: string;
    status: string;
    username?: string;
}

export interface TShiftCreate {
    start_time?: string;
    end_time?: string;
    comment?: string;
    isEnded?: boolean;
    location?: string;
    name?: string;
    shift_user_id?: string;
    status?: string;
    type?: string;
}

export interface TShiftTypes {
    id?: number;
    name?: string;
}
