export interface TUser {
    email: string;
    username: string;
    address: string;
    phone: string;
    status: string;
    nickname: string;
    bio: string;
    is_active: boolean;
    is_admin: boolean;
    id: number | string;
}
