import { defineStore } from 'pinia';
import {
    http_get_shift_by_month,
    http_create_shift,
    http_get_all_shift_types,
    http_create_shift_type,
    http_delete_shift_type,
    http_update_shift_type,
} from '@/services/shift.service';
import { TShift, TShiftTypes } from '@/types/shift_types';
import { useAlerts } from '@/store';

export type ShiftRootState = {
    shifts: TShift[];
    shift_types: TShiftTypes[];
};

export const useShift = defineStore({
    id: 'shift',
    state: () =>
        ({
            shifts: [],
            shift_types: [],
        } as ShiftRootState),
    actions: {
        async fetchShiftByMonth(month: string) {
            try {
                const { data } = await http_get_shift_by_month(month);
                if (data.length) {
                    this.$state.shifts = data;
                }
            } catch (e) {
                console.log(e);
            }
        },

        async createShift(shift: TShift) {
            try {
                await http_create_shift(shift);
            } catch (error) {
                console.log(error);
            }
        },

        // Shift types
        async fetchShiftTypes() {
            const store_alert = useAlerts();
            try {
                const { data } = await http_get_all_shift_types();
                this.$state.shift_types = data;
            } catch (error) {
                store_alert.alertError('Failed to fetch shift types.');
            }
        },
        async createShiftType(shift_type: TShiftTypes) {
            const alert = useAlerts();
            try {
                const ready_create = this.$state.shift_types.find(
                    (i) => i.name === shift_type.name
                );
                if (ready_create) {
                    alert.alertWarning(`Shift type ${shift_type.name} already exists.`);
                    return;
                }
                await http_create_shift_type(shift_type);
            } catch (error) {
                alert.alertError('Failed to create shift type.');
            }
        },
        async deleteShiftType(shift_type_id: number) {
            const alert = useAlerts();
            try {
                await http_delete_shift_type(shift_type_id);
            } catch (error) {
                alert.alertError('Failed to delete shift type.');
            }
        },
        async updateShiftType(shift_type: TShiftTypes) {
            const alert = useAlerts();
            try {
                await http_update_shift_type(shift_type);
            } catch (error) {
                alert.alertError('Failed to update shift type.');
            }
        },
    },
});
