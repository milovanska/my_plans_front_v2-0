export const ENDPOINTS = {
    users: {
        get_all_users: '/auth/users',
        get_user: '/auth/user',
        create_user: '/auth/sign-up',
    },

    auth: {
        signIn: '/auth/sign-in',
    },

    shifts: {
        default: '/shifts/',
        shifts_by_month: '/shifts/shifts-by-month/',
        shift_by_id: '/shifts/shift',
    },
    shift_type: {
        default: '/shifts/shift-type',
    },
};
