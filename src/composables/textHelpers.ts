export const useSliceTxt = () => {
    const sliceTxt = (txt: string, slice: number) =>
        txt ? (txt.length > slice ? txt.slice(0, slice) + '...' : txt) : '';

    return {
        sliceTxt,
    };
};
