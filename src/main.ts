import App from './App.vue';
import { createApp } from 'vue';
import { registerPlugins } from '@/plugins';

import './assets/styles/index.scss';
import VueTheMask from 'vue-the-mask';

const app = createApp(App);

registerPlugins(app);

app.use(VueTheMask);
app.mount('#app');
