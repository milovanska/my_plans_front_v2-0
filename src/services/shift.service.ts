import { http } from './http';
import { ENDPOINTS } from '@/constants/endpoints';
import { TShift, TShiftTypes } from '@/types/shift_types';

export const http_get_shift_by_month = async (month: string) => {
    return await http.get<TShift[]>(`${ENDPOINTS.shifts.shifts_by_month}${month}`);
};

export const http_create_shift = async (shift_data: TShift): Promise<TShift> => {
    return await http.post<TShift>(`${ENDPOINTS.shifts.default}`, shift_data);
};

// Shift types
export const http_get_all_shift_types = async () => {
    return await http.get<TShiftTypes[]>(`${ENDPOINTS.shift_type.default}`);
};
export const http_create_shift_type = async (shift_type: TShiftTypes): Promise<TShiftTypes> => {
    return await http.post<TShiftTypes>(`${ENDPOINTS.shift_type.default}`, shift_type);
};
export const http_delete_shift_type = async (type_shift_id: number) => {
    const url = `${ENDPOINTS.shift_type.default}/`;
    const config = {
        params: {
            type_shift_id,
        },
    };
    return await http.delete<TShiftTypes>(url, config);
};

export const http_update_shift_type = async (payload: TShiftTypes) => {
    return await http.put<TShiftTypes>(`${ENDPOINTS.shift_type.default}/${payload.id}`, {
        name: payload.name,
    });
};
